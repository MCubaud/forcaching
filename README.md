README FORCACHING :

Forcaching est un escape game en ligne mettant en scène une chasse au trésor à Forcalquier.

INSTALLATION :
Le jeu doit être hébergé sur un serveur web possédant un serveur MySQL. Il faut créer sur ce serveur MySQL une base de données forcaching.
Les tables s'installent automatiquement lors de la première utilisation.
Si ce n'est pas le cas, importez les tables etat_partie.sql et objets.sql dans la base de données forcaching.

Pour plus d'infos, voir Manuel.pdf.

![Screenshot](images/forcalquier.jpg)