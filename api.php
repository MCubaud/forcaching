<?php

// Paramètres pour se connecter à la base de données
$server = "localhost:3306";
$user="root";
$pass="root";
$bdd="forcaching";
$link = mysqli_connect($server, $user, $pass, $bdd);


/*Dans un premier temps, en fonction du besoin défini par $_POST['type'], on
construit la requête.
Dans un second temps, on exécute la requête. On sépare les requêtes qui doivent
renvoyer des données des requêtes qui ne font que modifier la base de données
(la variable $retour_necessaire permet de faire la distinction).
Il existe deux bases de données :
-objets qui contient la liste des objets, leur position et les liens qui les
relient entre eux.
-etat_partie qui contient la liste des parties en cours, et pour chaque parties
l'identifiant, le mot de passe et l'état de chaque objet (s'il a été ou non trouvé).*/

if ($_POST['type']=="info_balise") {
  /*Paramètres : 'numéro'
  Renvoie tous les champs de l'objet ayant pour id 'numéro'.*/
  $retour_necessaire=TRUE;
  $numero=$_POST['numero'];
  $requete = "SELECT * FROM objets WHERE id=".$numero;
}

elseif ($_POST['type']=="identifiant_cookie"){
  $retour_necessaire=TRUE;
  $identifiant=$_POST['identifiant'];
  if(setcookie('identifiant', $identifiant)){
    $requete="SELECT 1 FROM objets";
  }else{
    $requete="SELECT 0 FROM objets";
  }

}

elseif ($_POST['type']=="recuperer_identifiant_cookie"){
  $retour_necessaire=FALSE;
  $tableau_cookie = [];
  $tableau_cookie['identifiant']=$_COOKIE['identifiant'];
}

elseif ($_POST['type']=="creer_etat_partie") {
  /*Paramètres : aucun
  Crée la base de données etat_partie. La requête se trouve dans le fichier
  'etat_partie.txt'.*/
  $retour_necessaire=FALSE;
  $fp = fopen ("etat_partie.txt", "r");
  $requete = fread($fp, filesize('etat_partie.txt'));
  fclose ($fp);
}

elseif ($_POST['type']=="creer_objets") {
  /*Paramètres : aucun
  Crée la base de données objets. La requête se trouve dans quatre fichiers
  'objets_1.txt', 'objets_2.txt', 'objets_3.txt', 'objets_4.txt'. On ne peut
  exécuter visiblement les quatre opérations en même temps, d'où la séparation
  en quatre fichiers.
  */
  $retour_necessaire=FALSE;
  for ($i=1; $i <5 ; $i++) {
    $fichier="objets".$i.".txt";
    $fp = fopen ($fichier, "r");
    $requete = fread($fp, filesize($fichier));
    fclose ($fp);
    if ($i<4) {
      /*L'exécution du fichier 'objets_4.txt' se fait dans le deuxième temps car
      une requête y est toujours exécutée.*/
      $result = mysqli_query($link, $requete);
    }
  }
}

elseif ($_POST['type']=="mot_de_passe") {
  /*Paramètres : 'identifiant'
  Renvoie le mot de passe associé à 'identifiant'. Si l'identifiant n'existe
  pas, un tableau vide est renvoyé.*/
  $retour_necessaire=TRUE;
  $identifiant="'".$_POST['identifiant']."'";
  $requete="SELECT mot_de_passe FROM etat_partie WHERE identifiant=".$identifiant;
}

elseif ($_POST['type']=="compter_id") {
  /*Paramètres : aucun
  Renvoie le nombre des identifiants contenus dans etat_partie. Si etat_partie n'existe pas, -1 est renvoyé.*/
  $retour_necessaire=TRUE;
  $requete="SELECT count(*) as compte FROM etat_partie";
}
elseif ($_POST['type']=="obtenir_etat_partie") {
  /*Paramètres : 'identifiant'
  Renvoie 1 si la partie associée à identifiant est finie, 0 sinon.*/
  $retour_necessaire=TRUE;
  $identifiant="'".$_POST['identifiant']."'";
  $requete="SELECT partie_finie FROM etat_partie WHERE identifiant=".$identifiant;
}

elseif ($_POST['type']=="modifier_partie_finie") {
  /*Paramètres : identifiant
  Modifie dans etat_partie le score de identifiant par score.*/
  $retour_necessaire=FALSE;
  $score=$_POST['score'];
  $identifiant="'".$_POST['identifiant']."'";
  $requete="UPDATE etat_partie SET partie_finie = 1 WHERE identifiant LIKE $identifiant";
}

elseif ($_POST['type']=="obtenir_score_id") {
  /*Paramètres : 'identifiant'
  Renvoie le score de la partie associée à identifiant.*/
  $retour_necessaire=TRUE;
  $identifiant="'".$_POST['identifiant']."'";
  $requete="SELECT score FROM etat_partie WHERE identifiant=".$identifiant;
}

elseif ($_POST['type']=="obtenir_tous_scores") {
  /*Paramètres : aucun
  Renvoie la liste de tous les scores des parties achevées avec leur identifiant.*/
  $retour_necessaire=TRUE;
  $requete="SELECT identifiant, score FROM etat_partie WHERE partie_finie=1 ORDER BY score ASC";
}

elseif ($_POST['type']=="modifier_score") {
  /*Paramètres : identifiant, score
  Modifie dans etat_partie le score de identifiant par score.*/
  $retour_necessaire=FALSE;
  $score=$_POST['score'];
  $identifiant="'".$_POST['identifiant']."'";
  $requete="UPDATE etat_partie SET score = $score WHERE identifiant LIKE $identifiant";
}

elseif ($_POST['type']=='nouvelle_partie') {
  /*Paramètres : 'identifiant', 'mot_de_passe'
  Crée une nouvelle instance dans la table etat_partie dont l'identifiant et le
  mot de passe sont définis par les paramètres. Tous les états des objets ont
  par défaut la valeur 'cache' ou 'affiche' selon l'objet.*/
  $retour_necessaire=FALSE;
  $identifiant="'".$_POST['identifiant']."'";
  $mot_de_passe="'".$_POST['mot_de_passe']."'";
  $requete="INSERT INTO etat_partie (identifiant, mot_de_passe) VALUES ($identifiant, $mot_de_passe)";
}

elseif ($_POST['type']=='modifier_etat_balise') {
  /*Paramètres : 'identifiant', 'numero_balise', 'nouvelle_valeur'
  Modifie dans la table etat_partie l'état de l'objet 'numero_balise' par
  'nouvelle_valeur' pour la partie dont l'identifiant est 'identifiant'.*/
  $retour_necessaire=FALSE;
  $identifiant="'".$_POST['identifiant']."'";
  $balise='balise_'.$_POST['numero_balise'];
  $valeur="'".$_POST['nouvelle_valeur']."'";
  $requete="UPDATE etat_partie SET $balise = $valeur WHERE identifiant LIKE $identifiant";
}

elseif ($_POST['type']=='etat_balise') {
  /*Paramètres : 'identifiant', 'numero_balise'
  Renvoie l'état de la balise 'numero_balise' dans la partie dont l'identifiant
  est 'identifiant'.*/
  $retour_necessaire=TRUE;
  $identifiant="'".$_POST['identifiant']."'";
  $balise='balise_'.$_POST['numero_balise'];
  $requete="SELECT ".$balise." FROM etat_partie WHERE identifiant=".$identifiant;
}

elseif ($_POST['type']=='liste_objet_affiche' or $_POST['type']=='liste_objet_recupere') {
  /*Paramètres : 'identifiant'
  Renvoie l'ensemble des informations des objets qui ont le statut 'affiche' ou le statut 'recupere'
  dans la partie dont l'identifiant est 'identifiant'.*/
  $retour_necessaire=TRUE;
  $identifiant="'".$_POST['identifiant']."'";
  /*On effectue une première requête dans laquelle on récupère tous les champs
  de la partie 'identifiant'. Ces champs spécifient notamment pour chaque objet
  s'il est en statut 'affiche' ou 'cache'.
  */
  $requete_intermediaire="SELECT * FROM etat_partie WHERE identifiant LIKE ".$identifiant;
  if ($result_intermediaire = mysqli_query($link, $requete_intermediaire)) {
    while ($ligne = mysqli_fetch_assoc($result_intermediaire)) {
      $tableau_intermediaire[]=$ligne;
    }
  }
  $tableau_intermediaire=$tableau_intermediaire[0];
  /*On parcourt tous les objets et s'ils ont le statut 'affiche', on ajoute dans
  la requête à exécuter dans le deuxième temps la condition pour les
  sélectionner.*/

  $condition="";
  $compte=0;

  if ($_POST['type']=='liste_objet_affiche') {
    $string="affiche";
  }
  else {
    $string="recupere";
  }
  for ($i=1; $i<22; $i++) {
    $numero_balise="balise_".$i;
    if ($tableau_intermediaire[$numero_balise]==$string) {
      if ($compte==0) {
        $condition=$condition." id=".$i;
        $compte+=1;
      }
      else{
        $condition=$condition." OR id=".$i;
      }
    }
  }
  if ($compte==0) {
    $tableau_non_recupere = [];
    echo JSON_encode($tableau_non_recupere);
    $retour_necessaire=FALSE;
  }
  $requete = "SELECT * FROM objets WHERE".$condition;

}


elseif ($_POST['type']=='nombre_objet_recupere') {
  /*Paramètres : 'identifiant'
  Renvoie l'ensemble des informations des objets qui ont le statut 'affiche' ou le statut 'recupere'
  dans la partie dont l'identifiant est 'identifiant'.*/
  $retour_necessaire=FALSE;
  $identifiant="'".$_POST['identifiant']."'";
  /*On effectue une première requête dans laquelle on récupère tous les champs
  de la partie 'identifiant'. Ces champs spécifient notamment pour chaque objet
  s'il est en statut 'affiche' ou 'cache'.
  */
  $requete_intermediaire="SELECT * FROM etat_partie WHERE identifiant LIKE ".$identifiant;
  if ($result_intermediaire = mysqli_query($link, $requete_intermediaire)) {
    while ($ligne = mysqli_fetch_assoc($result_intermediaire)) {
      $tableau_intermediaire[]=$ligne;
    }
  }
  $tableau_intermediaire=$tableau_intermediaire[0];
  /*On parcourt tous les objets et s'ils ont le statut 'affiche', on ajoute dans
  la requête à exécuter dans le deuxième temps la condition pour les
  sélectionner.*/

  $condition="";
  $compte=0;
  $string="recupere";
  for ($i=1; $i<22; $i++) {
    $numero_balise="balise_".$i;
    if ($tableau_intermediaire[$numero_balise]==$string) {
      $compte+=1;
    }
  }
  $tableau_non_recupere=[];
  $tableau_non_recupere["compte"]=$compte;
  echo JSON_encode($tableau_non_recupere);


}

/*Deuxième temps : on exécute les requêtes. D'abord on exécute celles qui
doivent renvoyer des données, puis celles qui ne font que modifier les bases de
données.*/
if ($retour_necessaire){
  $tableau=[];
  $result = mysqli_query($link, $requete);
  if ($result) {
    while ($ligne = mysqli_fetch_assoc($result)) {
      $tableau[]=$ligne;
    }
    /*On modifie le tableau résultant de la requête pour lui donner le format
    voulu et pour que les valeurs correspondent bien à leur type (entier,
    flottant, chaîne de caractère).*/
    /* Champ dont la valeur est un nombre entier.*/
    $valeur_entiere=["id", "zoom", "recup", "bloque_par_objet", "piste", "affiche_objet", "largeur_image", "hauteur_image", "ancreX", "ancreY","compte","partie_finie","score"];
    /*Champ dont la valeur est un nombre flottant.*/
    $valeur_flottante=["latitude", "longitude"];
    /*Si la requête ne renvoie rien, on retourne un tableau vide.*/
    if (sizeof($tableau)==0){
      $nouveau_tableau=[];
    }
    else{
      foreach ($tableau as $ligne_tab) {
        $nouvelle_ligne=[];
        foreach ($ligne_tab as $key => $value) {
          if (in_array($key, $valeur_entiere)) {
            $nouvelle_ligne[$key]=intval($value);
          }
          elseif (in_array($key, $valeur_flottante)) {
            $nouvelle_ligne[$key]=floatval($value);
          }
          else {
            $nouvelle_ligne[$key]= $value;
          }
        }
        $nouveau_tableau[]=$nouvelle_ligne;
      }
    }
  echo JSON_encode($nouveau_tableau);
  }
elseif ($_POST['type']=="compter_id") {
  $key="compte";
  $nouvelle_ligne=[];
  $nouvelle_ligne[$key]=-1;
  $nouveau_tableau[]=$nouvelle_ligne;
  echo JSON_encode($nouveau_tableau);
}
}
elseif ($_POST['type']=="recuperer_identifiant_cookie") {

  echo JSON_encode($tableau_cookie);
}
elseif ($_POST['type']=='liste_objet_affiche' or $_POST['type']=='liste_objet_recupere' or $_POST['type']=='nombre_objet_recupere'){
  }
/*S'il n'y a rien à renvoyer, on exécute simplement la requête.*/
else {
  $result = mysqli_query($link, $requete);
}

?>
