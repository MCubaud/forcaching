///Le jeu est lancé.
lancerJeu();

///affichage de la carte
mymap = L.map('mapid').setView([43.957, 5.78], 15);
layer=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',maxZoom: 22, maxNativeZoom :19,
}).addTo(mymap);
Icone = L.Icon.extend({
    options: {
        shadowUrl: '',
        iconSize:     [42, 95],
        shadowSize:   [50, 64],
        iconAnchor:   [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor:  [-3, -76]
    }
});
//group contient tous les marqueurs, avec un marqueur par énigme.
group=L.featureGroup().addTo(mymap);
mymap.addEventListener("zoomend",function(e){
    /*Met à jour la carte en cas de zoom.*/
    mettre_a_jour_carte();
});
mymap.addEventListener("moveend",function(e){
    /*met à jour l'ambiance en cas de déplacement dans la carte*/
    mettre_a_jour_ambiance();
});


//Variables globales.
var inventaire=document.getElementById("inventaire");
var codes=document.getElementById("codes");
var HallOfFame=document.getElementById("HallOfFame");
var quitter=document.getElementById("Quitter");
var identifiant="";
var score=0;
var score_precedant=0;
temps_debut=Date.now();
var listeObjetsRecuperes=[];
var listeObjetsPresents=[];
var victoire=false;


//Evénements.
HallOfFame.addEventListener("click",function(e){
    /*Si on clique sur "Hall of Fame", on affiche la page "Forcaching3.html"*/
    if (!victoire){
      /*Si on quitte la partie, on met à jour le score de la partie.*/
        mettraAJourScore(identifiant,Math.floor((Date.now()-temps_debut)/1000+score_precedant));
    }
    window.location.href="forcaching3.html";
})

quitter.addEventListener("click",function(e){
  /*Si on clique sur "Quitter", on affiche la page d'accueil "index.html"*/
    if (!victoire){
      /*Si on quitte la partie, on met à jour le score de la partie.*/
        mettraAJourScore(identifiant,Math.floor((Date.now()-temps_debut)/1000+score_precedant));
    }
    window.location.href="index.html";
})


///Musique du jeu :
document.getElementById("music").play();
document.getElementById("music").loop=true;
document.getElementById("ville").volume=0.5;
document.getElementById("jour").volume=0.5;
document.getElementById("nuit").volume=0.5;
var volume=document.getElementById("volume");
volume.addEventListener("click",function(e){
    volume.value=Math.ceil((e.x-volume.offsetLeft)/volume.clientWidth*100);
    ["music","win","no","snap","near"].forEach(element=>{
        document.getElementById(element).volume=(e.x-volume.offsetLeft)/volume.clientWidth;
    });
    ["ville","jour","nuit"].forEach(element=>{
        document.getElementById(element).volume=0.5*(e.x-volume.offsetLeft)/volume.clientWidth;
    });
})

/*Les fonctions suivantes permettent d'initialiser la partie*/
function lancerJeu(){
    /*Récupère l'identifiant de la partie dans les cookies.*/
    var data = 'type=recuperer_identifiant_cookie';
    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
    .then(r=>r.json())
    .then(r=>lancerJeu2(r))
}

function lancerJeu2(r){
    /*Charge tous les paramètres de la partie*/
    identifiant=r.identifiant;
    /*Ajout d'un événement qui permet de sauvegarder le score lorsque l'on ferme la page.*/
    window.addEventListener("beforeunload",function(e){fermeture(e)});
    /*Récupère le temps de la partie en cours.*/
    recupererTempsPrecedant(identifiant);
    /*Teste si la partie a déjà été finie.*/
    tester_victoire(identifiant);
    /*Récupère les objets qui doivent être affichés.*/
    recupererObjetsAAfficher(identifiant);
    /*Récupère les objets qui ont déjà été ramassés pour les afficher dans la barre du bas.*/
    recupererObjetsARecuperer(identifiant);
    /*Petit effet de dé-zoom.*/
    setTimeout(function () {
        mymap.setZoom(13);
    }, 1000);
    /*Ajout d'Iris et Pauline*/
    initialiserIrisPauline();
}

function fermeture(e){
    /*Sauvegarde le score lorsque l'on quitte la page*/
    var e = e || window.event;
    e.preventDefault();
    score=Math.floor((Date.now()-temps_debut)/1000+score_precedant);
    mettraAJourScore(identifiant,score);
    e.returnValue= "";
}

function recupererTempsPrecedant(identifiant){
    /*Récupère au début de la partie l'ancienne valeur de score sauvegardée pour le joueur identifiant*/
    var data = 'type=obtenir_score_id&identifiant='+identifiant;
    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
    .then(r=>r.json())
    .then(r=>{
            score_precedant=r[0].score;
        })
}

function tester_victoire(identifiant){
    /*Vérifie si la partie a déjà été terminée. La variable victoire sert à
    déterminer s'il faut mettre à jour ou non le score lorsque l'on trouve le trésor.*/
     var data = 'type=obtenir_etat_partie&identifiant='+identifiant;
     fetch('api.php', {
     method: 'post',
     body: data,
     headers: {
         'Content-Type': 'application/x-www-form-urlencoded'
     }
     })
     .then(r=>r.json())
     .then(r=>{
         victoire=r.partie_finie;
     })
 }

 function recupererObjetsAAfficher(identifiant){
     /*Permet de récupérer sur le serveur les objets pour lesquels l'état de la partie est "affiche"*/
     var data = 'type=liste_objet_affiche&identifiant='+identifiant;
     fetch('api.php', {
     method: 'post',
     body: data,
     headers: {
         'Content-Type': 'application/x-www-form-urlencoded'
     }
     })
     .then(r=>r.json())
     .then(r=>r.forEach(objet=>listeObjetsPresents.push(objet)))
     .then(r=>mettre_a_jour_carte())
 }

 function recupererObjetsARecuperer(identifiant){
     /*Permet de récupérer sur le serveur les objets pour lesquels l'état de la partie est "recupere"*/
     var data = 'type=liste_objet_recupere&identifiant='+identifiant;
     fetch('api.php', {
     method: 'post',
     body: data,
     headers: {
         'Content-Type': 'application/x-www-form-urlencoded'
     }
     })
     .then(r=>r.json())
     .then(r=>{
         if (r!=[]) {
           r.forEach(objet=>recupererObjet(objet));
         }
     })
     .then(r=>mettre_a_jour_carte())
 }

 function mettre_a_jour_carte(){
     /*Affiche uniquement les objets presents pour lesquels le zoom minimum est inférieur au zoom de la carte*/
     group.clearLayers();
     listeObjetsPresents.forEach(function(objet){
         if(mymap._zoom>objet.zoom){
             ajouterObjetCarte(objet);
         }
     })
 }

 function ajouterObjetCarte(objet){
     /*Permet d'ajouter à la carte un nouvel objet*/
     var image= new Icone({iconUrl: "images/"+objet.image, iconSize: [objet.largeur_image,objet.hauteur_image], iconAnchor: [objet.ancreX,objet.ancreY]})
     // var marker = L.marker([objet.latitude, objet.longitude],{icon:image},{riseOnHover:true}).addTo(group);
     var marker = L.marker([objet.latitude, objet.longitude],{icon:image}).addTo(group);
     marker.addEventListener("click",function(e){
         ///en cas de click sur cet objet
         evenementAjouterObjetCarte(objet,marker,e);
     })
 }







/*Les fonctions suivantes correspondent au déroulement du jeu.*/
function evenementAjouterObjetCarte(objet,marker,e){
  /*Des événements sont associés à un click sur les marqueurs.*/
  marker.unbindPopup();
  if(objet.bloque_par_objet){
      /*Si l'objet est bloqué par un objet, on vérifie que l'on dispose de l'objet qui le débloque.*/
      actionBloqueParObjet(objet,marker,e);
  }else{
      if(objet.bloque_par_code){
          /*Si l'objet est bloqué par un code, on demande à l'utilisateur de rentrer le code.*/
          actionBloqueParCode(objet,marker,e);
      }else{
          /*Si l'objet n'est pas bloqué.*/
          actionsPasBloque(objet,marker, e);
      }
  }
}

/*Fonctions lorsqu'un objet est bloqué par un autre objet.*/
function actionBloqueParObjet(objet,marker,e){
    /*Actions si l'objet est bloqué par un objet.
    Si on possède l'objet le débloquant on le débloque, sinon on affiche son indice s'il en a un.*/
    if(possedeObjetDebloquant(objet)){
        /*Si on possède l'objet le débloquant, alors on le débloque.*/
        debloquer(objet,marker,e);
    }
    else{
        /*Si on ne possède pas l'objet le débloquant, alors on affiche l'indice s'il y en a un.*/
        /*On joue une petite musique qui fait comprendre que le joueur ne peut débloquer l'objet.*/
        document.getElementById("no").play();
        console.log("objet bloquant :")
        ObjetEnConsole(objet.bloque_par_objet);//Nous n'affichons pas directement l'objet qui bloque car ça n'a pas de sens dans les mécanismes de jeu mis en place,
        // mais on l'affiche en console pour d'une part respecter le sujet et d'autre part montrer qu'il ne s'agit pas d'une incapacité technique de notre part;
        if(objet.fournit_indice){
            marker.bindPopup("<b>indice</b><br>"+objet.fournit_indice).openPopup();
    }}
}

function possedeObjetDebloquant(objet){
    /*renvoie true si le joueur possede l'objet qui permet de débloquer cet objet*/
    possede=false;
    listeObjetsRecuperes.forEach(function(objRecup){
        if (objet.bloque_par_objet==objRecup.id){
            possede=true;
        }
    })
    return possede;
}

function debloquer(objet,marker,e){
    /*debloque cet objet et appelle la fonction actionPasBloque() */
    objet.bloque_par_objet=0;
    actionsPasBloque(objet,marker,e);
}


/*Fonctions lorsqu'un objet est bloqué par un code.*/
function actionBloqueParCode(objet,marker,e){
    /* Actions si l'objet est bloqué par un code :
    - on fabrique une petite fenêtre code dans laquelle on affiche l'indice s'il existe, une barre pour rentrer le code et un bouton ok
    - en cas de click sur ok, on teste si le code entré est bon*/
    texte="";
    if(objet.fournit_indice){
        /*S'il y a un indice, on l'affiche.*/
        texte+="<p><b>indice</b> : "+objet.fournit_indice+"</p>";
    }
    texte+="<form id='formulaire' method='post'>";
    texte+="<input id='code' type='text'>";
    texte+="<input id='valider' type='submit' value='OK' />";
    texte+="</form>";
    marker.bindPopup(texte).openPopup();
   var buttonSubmit = L.DomUtil.get('valider');
   var entree = L.DomUtil.get('code');
   /*Ajout de l'événement lorsque l'on envoie le formulaire.*/
   L.DomEvent.addListener(buttonSubmit, 'click',function(event){
       event.preventDefault();
       if(entree.value==objet.bloque_par_code){
           /*Si le code rentré est le bon, alors on débloque l'objet.*/
           entree.style.color="green";
           setTimeout(function(){debloquer(objet,marker,e);},50);
       }else{
           /*Si le code rentré n'est pas le bon, alors on joue une petite musique
           et on vide la barre dans laquelle on rentre le code.*/
           document.getElementById("no").play();
           entree.style.color="red";
           setTimeout(function(){entree.value="";entree.style.color="black";},500);
       }
   });
    /*Ajout d'un événement si l'on clique ailleurs. On vide alors le code.*/
    
}

function vider(partie){
    /*permet de retirer tous les enfants html de ce parent et de le rendre transparent*/
    while(partie.children.length>0){
        partie.removeChild(partie.children[0]);
        partie.style.backgroundColor="rgba(250,250,250,0)";
    }
}



/*Les fonctions suivantes concernent lorsqu'un objet n'est pas bloqué.*/
function actionsPasBloque(objet,marker, e){
    /*Actions possibles si l'objet n'est pas bloqué*/
    if (objet.fournit_code){
        /*Si l'objet fournit un code, alors on l'affiche dans une popup.*/
        marker.bindPopup("code: "+objet.fournit_code).openPopup();
        if(objet.affiche_objet){
            /*Si l'objet affiche un autre objet, alors on l'affiche.*/
            afficherObjet(objet);
        }
        if(!marker.isPopupOpen()){
            //il semble que parfois ça ne veuille pas s'ouvrir convenablement
            document.getElementById("snap").play();
            marker.getPopup().openOn(mymap);
        }
        
    }
    else if (objet.fournit_enigme) {
      /*Si l'objet fournit une énigme, alors on l'affiche dans une popup.*/
      /*On met en forme le texte à afficher dans la popup.*/
      marker.off();//on désactive tous les eventListener actuellement présents sur le marker.
      document.getElementById("snap").play();
      let enigme=objet.fournit_enigme;
      let listeVers=enigme.split("\r\n");
      let texte="<div class='popup_enigmes'><b>Enigme : </b></div>";
      listeVers.forEach(vers=>{
        texte+="<div class='popup_enigmes'>"+vers+"</div>";
      });
      marker.bindPopup(texte).openPopup();
      /*Dès que la popup se ferme, on récupère l'objet et on affiche l'objet suivant s'il y en a un à afficher.*/
      function clickAilleurs2(){
        marker.removeEventListener("popupclose",clickAilleurs2);
        recupererObjet(objet);
        if(objet.affiche_objet){
            afficherObjet(objet);
          }
        }
      setTimeout(e=>marker.addEventListener("popupclose",clickAilleurs2),500);
    }
    else if (objet.recup) {
        /*Si l'objet ne fournit ni une énigme, ni un code mais qu'il est récupérable, alors on le récupère.*/
        document.getElementById("snap").play();
        recupererObjet(objet);
    }
    else if(objet.affiche_objet){
        /*Si l'objet permet seulement d'afficher un autre objet, alors on l'affiche.*/
        afficherObjet(objet);
    }
}

function recupererObjet(objet){
     /*Enleve cet objet de la liste des objets présents, l'ajoute à celle des objets
     récupérés et l'affiche dans l'inventaire. Cette fonction sert tout aussi bien
     au chargement de la partie qu'à son déroulement.*/
     /*Ajoute l'objet dans la liste des objets récupérés.*/
     listeObjetsRecuperes.push(objet);
     /*Retire l'objet de la liste des objets affichés.*/
     listeObjetsPresents=listeObjetsPresents.filter(element=>(element.id !=objet.id))
     /*Affiche l'objet dans l'inventaire.*/
     let image=document.createElement("img");
     image.src="images/"+objet.image;
     image.height=42;
     image.style.margin="0px 3px";
     image.addEventListener("mouseover",function(){
         image.style.border="1px solid black";
     });
     image.addEventListener("mouseout",function(){
        image.style.border="0px";
     })
     inventaire.appendChild(image);
     mettre_a_jour_carte();
     /*On modifie l'état de la balise dans la table etat_partie.*/
     modifierEtatBalise(identifiant,objet.id,"recupere");
     /*Si l'objet fournit une énigme, on affiche l'énigme à droite.*/
     if (objet.fournit_enigme){
         if(objet.piste){
              /*Chaque chemin dispose d'un emplacement nommé piste. Cette piste
              affiche la dernière énigme trouvée par le joueur.*/
             let piste=document.getElementById("piste_"+objet.piste);
             let enigme=objet.fournit_enigme;
             let listeVers=enigme.split("\r\n")
             /*Vide la piste de l'énigme précédente.*/
             vider(piste);
             /*Affiche l'énigme dans la piste correspondante.*/
             piste.style.backgroundColor="burlywood";
             piste.style.padding="10px 10px";
             piste.style.border="1px solid black";
             listeVers.forEach(vers=>{
                 let ligne=document.createElement("p");
                 ligne.innerHTML=vers;
                 ligne.style.margin="0px";
                 piste.appendChild(ligne);
             });
         }
     }
     /*On regarde si l'objet récupéré permet de gagner la partie.*/
     verifier_victoire(objet);
 }

 function afficherObjet(objet){
     /*Ajoute l'objet que cet objet affiche à la liste des objets affichés et modifie l'état de la partie*/
     listeObjetsPresents=listeObjetsPresents.filter(element=>(element.id !=objet.affiche_objet))
     recupererObjetParId(objet.affiche_objet);
     modifierEtatBalise(identifiant,objet.affiche_objet,"affiche");
     mettre_a_jour_carte();
     objet.affiche_objet=0;
 }

function recupererObjetParId(id){
    /*Recherche sur le serveur l'objet ayant cet id et l'affiche en le mettant dans listeObjetsPresents.*/
    var data = 'type=info_balise&numero='+id;

    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
    .then(r => r.json())
    .then(r => {
    listeObjetsPresents.push(r[0]);
    mettre_a_jour_carte();
    return r[0];
    })
}

function modifierEtatBalise(identifiant,numeroBalise,nouvelle_valeur){
    /*Change sur le serveur dans la table etat_partie pour le joueur identifiant la valeur de la balise numero_balise par cette nouvelle_valeur*/
    if(identifiant){
        var data = 'type=modifier_etat_balise&identifiant='+identifiant+"&numero_balise="+numeroBalise+"&nouvelle_valeur="+nouvelle_valeur;
        fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        })
    }
}


/*Fonctions pour la fin de la partie.*/
function verifier_victoire(objet){
    /*Teste si cet objet est le trésor*/
    if (objet.id==20){
        document.getElementById("win").play();
        if (!(victoire)){
            score=Math.floor((Date.now()-temps_debut)/1000+score_precedant);
        }else{
          //Si cet identifiant a déjà gagné, on conserve le score précédant.
          score=score_precedant
        }
        setTimeout(function(){FinirPartie(identifiant,score);},1000)//Afin de laisser le temps à la musique d'être jouée.
    }
}

function FinirPartie(identifiant,score){
    /*Met à jour le score et passe la valeur de partie_finie à 1 dans etat_partie*/
    mettraAJourScore(identifiant,score);
    var data = 'type=modifier_partie_finie&identifiant='+identifiant+'&score='+score;
    fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(r=>creerCookie(identifiant))//On ajoute un cookie contenant le nom de l'utilisateur
    .then(r=>{
        setTimeout(function(){
            window.fermeture=function(e){console.log("Je ferme la page !")}///Le score ne se mettra plus à jour en quittant la page
            window.location.href="forcaching3.html";
        },500);
        
    })
}

function mettraAJourScore(identifiant,score){
    /*Met à jour le score pour le joueur identifiant avec la nouvelle valeur score*/
    var data = 'type=modifier_score&identifiant='+identifiant+'&score='+score;
    fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}
function creerCookie(identifiant){
    /*Ajoute un cookie au nom de l'utilisateur*/
    var data = 'type=identifiant_cookie&identifiant='+identifiant;
    fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}
function ObjetEnConsole(id){
    /*Affiche en console le nom de l'objet d'id id*/
    var data = 'type=info_balise&numero='+id;

    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
    .then(r => r.json())
    .then(r => {
    console.log(r[0].nom);
    })
}

function mettre_a_jour_ambiance(){
    /*on calcule la distance en fonction du centre de forcalquier, et on joue selon un son different*/
    center=mymap.getCenter();
    distance_tresor=Math.sqrt((center.lat-43.983368)*(center.lat-43.983368)+(center.lng-5.809004)*(center.lng-5.809004));
    rayon_tresor=0.0008;
    if (distance_tresor<rayon_tresor && mymap._zoom>18 && listeObjetsPresents.filter(objet=>objet.id==20).length!=0){
        //On est proche du trésor, à son niveau de zoom et celui-ci est affiché sur la carte
        document.getElementById("music").pause();
        document.getElementById("near").play();
    }
    else{
        //on est loin du trésor
        document.getElementById("near").pause();
        document.getElementById("music").play();
        rayon=0.01;
        distance=Math.sqrt((center.lat-43.957)*(center.lat-43.957)+(center.lng-5.78)*(center.lng-5.78));
        if (distance>rayon){
            //on est à la campagne
            if (Math.floor(Date.now()/60000)%2==0){
                //si le temps actuel en minute est un multiple de 2, on est le jour
                document.getElementById("ville").pause();
                document.getElementById("nuit").pause();
                document.getElementById("jour").play();
            }
            else {
                //c'est la nuit
                document.getElementById("ville").pause();
                document.getElementById("jour").pause();
                document.getElementById("nuit").play();
            }
        }else{
            //on est en ville
            document.getElementById("jour").pause();
            document.getElementById("nuit").pause();
            document.getElementById("ville").play();
        }
    }
}

///Fonctions en rapport avec le déplacement d'Iris et Pauline, vos concurentes qui ne manqueront pas une occasion pour demander des conseils.
function initialiserIrisPauline(){
    /*crée un marqueur pour iris et Pauline*/
    var image= new Icone({iconUrl: "images/IrisPauline.png", iconSize: [45,30], iconAnchor: [0,0]})
    IrisPauline = L.marker([43.962446,5.773993],{icon:image},{riseOnHover:true}).addTo(mymap);
    IrisPauline.addEventListener("click",function(){
        /*Ce que disent Iris et Pauline lorsqu'on leur clique dessus !*/
        let ListePhrases=["Donnez nous un indice s'il vous plaît !",
        "Vous pouvez pas nous laisser comme ça, donnez nous une piste !",
        "C'est trop dur, on ne dort plus la nuit, aidez nous!!!",
        "On vous suivra jusqu'à ce que vous nous donniez un indice !",
        "On sait qu'on abuse, mais on est bloquées là, il nous faut un indice !",
        "C'est pas possible, on a cherché de partout, le prochain message ne peut pas être juste à côté de chez nous !",
        "On va insister tant que vous nous aurez pas donné de piste, vous finirez bien par nous dire quelque chose !"];
        let numero_phrase=Math.floor(Math.random()*ListePhrases.length);
        IrisPauline.bindPopup(ListePhrases[numero_phrase]);
    });
    mymap.addEventListener("mousemove",function(e){
        /*déplace Iris et Pauline sur la carte*/
        deplacerIrisPauline(e);
    });
}

function deplacerIrisPauline(e){
    /*déplace Iris et Pauline sur la carte, elles suivent la position du curseur dans la carte avec un temps de retard*/
    let latlng=e.latlng;
    setTimeout(function(){
        IrisPauline.setLatLng(latlng);
    },1000);
}
