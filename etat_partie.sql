-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 03, 2020 at 01:04 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forcaching`
--

-- --------------------------------------------------------

--
-- Table structure for table `etat_partie`
--

CREATE TABLE `etat_partie` (
  `identifiant` text NOT NULL,
  `mot_de_passe` text NOT NULL,
  `balise_1` char(15) NOT NULL DEFAULT 'affiche',
  `balise_2` char(15) NOT NULL DEFAULT 'affiche',
  `balise_3` char(15) NOT NULL DEFAULT 'affiche',
  `balise_4` char(15) NOT NULL DEFAULT 'cache',
  `balise_5` char(15) NOT NULL DEFAULT 'affiche',
  `balise_6` char(15) NOT NULL DEFAULT 'cache',
  `balise_7` char(15) NOT NULL DEFAULT 'cache',
  `balise_8` char(15) NOT NULL DEFAULT 'affiche',
  `balise_9` char(15) NOT NULL DEFAULT 'affiche',
  `balise_10` char(15) NOT NULL DEFAULT 'cache',
  `balise_11` char(15) NOT NULL DEFAULT 'cache',
  `balise_12` char(15) NOT NULL DEFAULT 'cache',
  `balise_13` char(15) NOT NULL DEFAULT 'affiche',
  `balise_14` char(15) NOT NULL DEFAULT 'cache',
  `balise_15` char(15) NOT NULL DEFAULT 'cache',
  `balise_16` char(15) NOT NULL DEFAULT 'affiche',
  `balise_17` char(15) NOT NULL DEFAULT 'affiche',
  `balise_18` char(15) NOT NULL DEFAULT 'cache',
  `balise_19` char(15) NOT NULL DEFAULT 'cache',
  `balise_20` char(15) NOT NULL DEFAULT 'cache',
  `balise_21` char(15) NOT NULL DEFAULT 'affiche',
  `score` int(11) NOT NULL DEFAULT '0',
  `partie_finie` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
