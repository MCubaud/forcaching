-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 14, 2020 at 11:25 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forcaching`
--

-- --------------------------------------------------------

--
-- Table structure for table `objets`
--

CREATE TABLE `objets` (
  `nom` text NOT NULL,
  `id` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `zoom` double NOT NULL,
  `image` text NOT NULL,
  `largeur_image` int(11) NOT NULL,
  `hauteur_image` int(11) NOT NULL,
  `ancreX` int(11) NOT NULL,
  `ancreY` int(11) NOT NULL,
  `recup` tinyint(1) NOT NULL,
  `fournit_code` text NOT NULL,
  `fournit_indice` text NOT NULL,
  `fournit_enigme` text NOT NULL,
  `bloque_par_objet` int(11) NOT NULL,
  `bloque_par_code` text NOT NULL,
  `affiche_objet` int(11) NOT NULL,
  `piste` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objets`
--

INSERT INTO `objets` (`nom`, `id`, `latitude`, `longitude`, `zoom`, `image`, `largeur_image`, `hauteur_image`, `ancreX`, `ancreY`, `recup`, `fournit_code`, `fournit_indice`, `fournit_enigme`, `bloque_par_objet`, `bloque_par_code`, `affiche_objet`, `piste`) VALUES
('payan', 1, 43.972186, 5.800449, 12, 'payan.png', 95, 42, 47, 21, 1, '', '', 'Perdu dans les hauteurs de la ville des arts,\r\nD\'un côté bercé du doux bruit de la cythare,\r\nDe l\'autre contemplant la joie de Siméon,\r\nJ\'espère vous aider dans votre mission.\r\n', 0, '', 2, 1),
('sommet_citadelle', 2, 43.957093, 5.782059, 18, 'citadelle.png', 42, 95, 30, 95, 1, '', 'Sur la citadelle de Forcalquier, ancienne villégiature des comtes de Provence, est construite la chapelle Notre-Dame de Provence, dont le carillon est renommé.', 'Voyageur, vous qui montez à la citadelle,\r\nDaignez prêter attention aux belles stèles\r\nQui vous rapelleront une bien sombre histoire\r\nEt évitez de trébucher deux fois le soir.\r\n', 1, '', 3, 1),
('Chemin_de_croix', 3, 43.957352, 5.781652, 18, 'chemin_croix.png', 42, 95, 21, 95, 1, '', 'Un chemin de croix entoure la Citadelle', 'Afin de trouver la suite, vos pas vous mènent\r\nA une chapelle homonyme d\'une gare londonienne\r\nSous le rocher pointant le château de Sauvan,\r\nQuatre mètres cachent la clef à l\'imprudent.\r\n', 2, '', 4, 1),
('Saint_Pancrace', 4, 43.953937, 5.784677, 18, 'saint_pancrace.png', 64, 95, 40, 95, 0, '0 : 0 ; 5 : 0', 'La chapelle Saint Pancrace porte le nom du patron de la ville', '', 3, '', 20, 1),
('Le Plan des Aires', 5, 43.938925, 5.79467, 12, 'plan_des_aires.png', 95, 42, 47, 21, 1, '', '', 'A mi-hauteur entre l\'aigle et la citadelle,\r\nAu détour d\'un chemin un jardin très discret,\r\nDe la terre cinq doigts s\'élancent vers le ciel,\r\nA leur racine vous obtiendrez mon secret.', 0, '', 6, 2),
('Le jardin caché', 6, 43.956628, 5.781429, 18, 'doigts.png', 42, 60, 21, 25, 1, '', 'Un arbre dans un jardin', 'Des garrigues, l\'humble demeure de pierre\r\nConserve les plus précieux secrets d\'hier.\r\nAux portes de la ville, protégeant son entrée,\r\nElle garde les serpents et divers herbacés.', 5, '', 7, 2),
('Borie', 7, 43.963337, 5.793832, 18, 'borie.png', 135, 120, 67, 60, 1, '', 'Les bories sont des cabanes de pierre séche qui servaient de granges ou d\'habitation.', 'Dans un vieux cloître, sur une place cachée,\r\nUn oiseau aux mille plumes de métal veille.\r\nAvec son terrible œil droit d\'acier il surveille\r\nle lieu où le message est dissimulé.', 6, '', 21, 2),
('L\'oiseau', 8, 43.959212, 5.781371, 18, 'oiseau.png', 42, 95, 21, 15, 0, '1 : 4', 'Une statue d\'oiseau', '', 7, '', 20, 2),
('Centre IGN', 9, 43.962446, 5.773993, 12, 'centre_IGN.png', 95, 75, 47, 37, 1, '', '', 'De mon emplacement, au pied d\'un cadenas,\r\nLe feu, la foudre et la poudre pulvérisèrent\r\nLa pauvre ville de Forcalquier qui montra\r\nQue d\'un roi il ne faut susciter la colère.', 0, '', 10, 3),
('La Bombardière', 10, 43.956385, 5.786428, 18, 'Bombardiere.png', 135, 145, 67, 77, 1, '', 'La colline porte le nom de Bombardière, non sans raison.', '37-11 79-47-97-2-17-11-73-61 7-47-23-71 67-2-79-47-23-61 53-47-73-67-67-11-61 37\'2-61-71 7-11 37\'23-43-7-11-5-23-67-23-47-43 29-73-67-59-73\'2 5-11 59-73-11 37-2 5-19-2-43-5-11 37-11 61-2-71-71-61-2-53-11.» 13-61-2-43-5-47-23-67 71-47-73-61-2-43-11', 9, '', 11, 3),
('L\'indécis', 11, 43.958279, 5.780958, 18, 'indecis.png', 48, 85, 24, 85, 1, '', 'Un panneau indiquant la Citadelle.', 'Au lieu où tranquillement se reposent les morts\r\nAu détour d\'une allée le nombre de Moselle\r\nIndique la piste, le chemin du trésor,\r\nLa où un vieux révolutionnaire appelle.', 10, '', 12, 3),
('Le cimetière', 12, 43.964604, 5.786252, 18, 'cimetiere.png', 42, 95, 21, 95, 0, '2: 0 ; 7: a', 'Tombe de la famille Bouche', '', 11, '', 20, 3),
('Le pont roman', 13, 43.929998, 5.756521, 12, 'pont_roman.png', 160, 75, 80, 0, 1, '', '', 'Dans un petit jardin, vous pourrez me trouver,\r\nDes têtes de pierre, sur le mur opposé,\r\nDaignent me tenir agréable compagnie\r\nEt Horace pour moi écrit des vers jolis.', 0, '', 14, 4),
('Horace', 14, 43.958473, 5.781941, 18, 'horace.png', 100, 40, 50, 20, 1, '', 'Une plaque sur laquelle est écrit en latin : Multa Renascentur Quae Iam Cecidere', 'D\'un nombre sans rime, il te faudra compter,\r\nDe la seconde porte d\'une tour carrée\r\nD\'un ancien château de l\'évêché, les marches\r\nDu chemin qui des entrées longe les arches.', 13, '', 15, 4),
('La tour', 15, 43.957205, 5.782717, 18, 'escalier.png', 80, 80, 40, 40, 1, '', 'Ruines d\'un château', 'Sous les pieds du divin, chercher n\'est jamais vain\r\nCar du plus haut sommet, l\'anfractuosité\r\nÀ la forme carrée, la grotte si sacrée\r\nConserve dans son sein l\'indice bientôt  tien.', 14, '', 16, 4),
('Le trou carré', 16, 43.956963, 5.782024, 18, 'trou_carre.png', 80, 80, 40, 40, 0, '3: 8', 'Chapelle troglodyte sous la Citadelle', '', 15, '', 20, 4),
('Les Mourres en espadrille', 17, 43.980097, 5.772858, 12, 'mourres.png', 100, 95, 50, 47, 1, '', '', 'Si j\'étais où je vis, je n\'aurais pas péri\r\ncar cette eau à mes pieds peut éteindre un bûcher.\r\nDerrière mon nom je suis, caché par des écrits,\r\nVous allez me trouver, ce n\'est pas compliqué.', 0, '', 18, 5),
('Jeanne d\'Arc', 18, 43.957236, 5.77979, 18, 'jeanne.png', 50, 95, 30, 80, 1, '', 'Fontaine de Jeanne d\'Arc', 'Curieuse bâtisse que celle où je réside\r\nPuisque mon maître deux siècles plus tard est né,\r\nQui auprès de l\'Empereur fut à Saint-Dizier\r\net à La Porte contre les voisins perfides.', 17, '', 19, 5),
('Hotel de Sébastiani', 19, 43.958973, 5.781944, 18, 'hotel_de_sebastiani.png', 48, 95, 24, 96, 0, '4 : 8 ; 6 : 8', 'Porte de l\'hôtel de Sebastiani', '', 18, '', 20, 5),
('Tresor', 20, 43.983368, 5.809004, 18, 'tresor.png', 80, 95, 40, 47, 1, '', 'Pour m\'ouvrir, il vous faudra les 5 clés qui permirent de m\'atteindre', 'Vous avez réussi à trouver le trésor\r\nQui ne se résume point en quelques pièces d\'or\r\nMais en une gloire qui vous perdurera\r\nEt des siècles durant vous illuminera !', 0, '0408808a', 0, 0),
('oiseau_piege', 21, 43.959323, 5.782853, 18, 'oiseau_piege.png', 50, 50, 30, 30, 0, '', 'Une statue représentant un oiseau', 'Mon frère est à trouver, vous vous êtes trompés\r\nAu mauvais lieu vous êtes et vous vous sentez bêtes.\r\nComme moi il n\'a de crêtes, de fer est son squelette.\r\nNon pas aux Cordeliers, plus haut il faut aller.', 7, '', 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `objets`
--
ALTER TABLE `objets`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
