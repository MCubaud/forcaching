///Création des tables si elles n'existent pas
testerExistanceTables();

var Menu=document.getElementById("Menu");
var inventaire=document.getElementById("inventaire");
var codes=document.getElementById("codes");
var HallOfFame=document.getElementById("HallOfFame");
var identifiant="";
var score=0;
var score_precedant=0;

///Musique du jeu :
document.getElementById("music").play();
document.getElementById("music").loop=true;


Menu.addEventListener("submit",function(e){
    /*Lorsque l'on clique sur jouer, le formulaire est envoyé et les identifiants vont être testés.*/
    e.preventDefault();
    identifiant=document.getElementById("Pseudo").value;
    mdp=document.getElementById("mdp").value;
    testerIdentifiant(identifiant,mdp);
})

HallOfFame.addEventListener("click",function(e){
  /*Lorsque l'on clique sur le bouton Hall of Fame, la page du Hall of Fame s'ouvre.*/
    window.location.href="forcaching3.html";
})


/*Les fonctions suivantes concernent l'identification de l'utilisateur.On teste
si l'identifiant est déjà présent dans la table. Si tel est le cas, on vérifie
que le mot de passe entré est identique à celui de la base de données : on lance
alors le jeu ou on affiche une alerte. Si tel n'est pas le cas, on crée une
nouvelle partie en ajoutant une ligne dans la table état_partie.*/
function testerIdentifiant(identifiant,mdp){
    /*Teste si un identifiant existe, et si le mot de passe est correct*/
    /*On fait une requête pour récupérer le mot de passe associé à l'identifiant.*/
    var data = 'type=mot_de_passe&identifiant='+identifiant;
    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
    .then(r=>r.json())
    .then(r => {
            if (r.length == 0) {
                /*Si la requête renvoie un tableau vide, alors l'identifiant.
                n'existait pas et on crée un nouvelle partie avec l'identifiant et le mot de passe donné.*/
                register(identifiant, mdp);
                setTimeout(lancerJeu,200);//Pour laisser le temps d'enregistrer.
            } else {
                /*Sinon, on vérifie que les mots de passe correspondent.*/
                mdp_enregistre = r[0].mot_de_passe;
                if (mdp_enregistre == mdp) {
                    /*Si les mots de passe correspondent, on lance le jeu.*/
                    document.getElementById("mdp").style.color="green";
                    setTimeout(lancerJeu,100);//Pour laisser le temps de voir le magnifique effet.
                } else {
                    /*Si les mots de passe ne correspondent pas, on affiche une alerte.*/
                    document.getElementById("no").play();
                    alert("Mot de passe incorrect !!!");
                    document.getElementById("mdp").style.color="red";
                    setTimeout(function(){document.getElementById("mdp").value="";document.getElementById("mdp").style.color="black";},500);
                }
            }
        })
}

function lancerJeu(){
    /*On stocke dans les cookies le nom de l'identifiant, puis on ouvre la page "forcaching2.html".*/
    var data = 'type=identifiant_cookie&identifiant='+identifiant;
    fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(r=>{
        window.location.href="forcaching2.html";
    })
}

function register(identifiant,mot_de_passe){
    /*Crée un nouvel utilisateur avec cet identifiant et ce mot_de_passe.
    La partie serveur crée une nouvelle ligne dans la base de données etat_partie.*/
    var data = 'type=nouvelle_partie&identifiant='+identifiant+"&mot_de_passe="+mot_de_passe;
    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
}




//Les fonctions suivantes servent à construire les tables des bases de données si elles n'existent pas encore.

function testerExistanceTables(){
    /*Vérifie l'existance des tables dans la base de données forcaching.*/
    var data = 'type=compter_id';
    fetch('api.php', {
        method: 'post',
        body: 'type=compter_id',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(r=>r.json())
    .then(r=>{
        if (r[0].compte==-1){
            ///Si les tables n'existent pas, elles sont créées.
            creerEtatPartie();
            creerTableObjets();
        }
    })
}

function creerEtatPartie(){
    /*Crée la table etat_partie dans la base de données forcaching*/
    var data = 'type=creer_etat_partie';
    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
}

function creerTableObjets(){
    /*Crée la table objets dans la base de données forcaching.*/
    var data = 'type=creer_objets';
    fetch('api.php', {
    method: 'post',
    body: data,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    })
}
