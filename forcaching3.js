///Musique du jeu :
document.getElementById("music").play();
document.getElementById("music").loop=true;
identifiant_vainqueur="";

function ajouterScoreVainqueurs(){
    /*Ajoute les scores des vainqueurs au tableau des scores*/
    var data = 'type=obtenir_tous_scores';
    fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(r=>r.json())
    .then(r=>{
        let ligne1=document.createElement("tr");
        let colone_pseudo=document.createElement("th");
        colone_pseudo.innerHTML="pseudo";
        let colone_score=document.createElement("th");
        colone_score.innerHTML="temps";
        ligne1.appendChild(colone_pseudo);
        ligne1.appendChild(colone_score);
        liste_gagnant.appendChild(ligne1);
        r.forEach(function(element){
            /*Pour chaque partie terminée, on crée une nouvelle ligne avec l'identifiant et le score.*/
            ligne=document.createElement("tr");
            pseudo=document.createElement("td");
            pseudo.innerHTML=element.identifiant;
            ligne.appendChild(pseudo);
            score=document.createElement("td");
            score.innerHTML=element.score;
            ligne.appendChild(score);
            liste_gagnant.appendChild(ligne);
            if (element.identifiant==identifiant_vainqueur){
                pseudo.style.backgroundColor="indianred";
                score.style.backgroundColor="indianred";
            }
        });
    })
}
function recuperer_cookie(){
    var data = 'type=recuperer_identifiant_cookie';
    fetch('api.php', {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(r=>r.json())
    .then(r=>{
        identifiant_vainqueur=r.identifiant;
        console.log("il vient de gagner !");
    })
}



Scoreboard=document.getElementById("Scoreboard");
liste_gagnant=document.createElement("table");
Texte=document.getElementById("texte");
Scoreboard.insertBefore(liste_gagnant, Texte);
recuperer_cookie();
ajouterScoreVainqueurs();



Quitter=document.getElementById("Quitter");
Quitter.addEventListener("click",function(e){
    window.location.href="index.html";
});
